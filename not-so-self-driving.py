import sys
import math
import string

# Defining "constant" variables to avoid magic numbers
SWERVE_TIME: int = 1
BRAKE_TIME: int = 5

# Loading number of cases
cases: int = int(sys.stdin.readline().rstrip())

# Iterating through cases
for caseNum in range(cases):
    # Format: "speed:distance"
    concatenated_car_data: str = sys.stdin.readline().rstrip()
    # Format: [speed, distance]
    split_car_data: list[str] = concatenated_car_data.split(':')
    speed: float = float(split_car_data[0])
    distance: float = float(split_car_data[1]

    # Formula: speed (m/s) * time (s) = distance (m)

    # Collide in 1 second or less (SWERVE_TIME)
    # If speed * 1 second >= distance, then we must SWERVE
    if speed * SWERVE_TIME >= distance:
        print("SWERVE")
    # Collide in 5 seconds or less (BRAKE_TIME)
    # If speed * 5 seconds >= distance, then we must BRAKE
    elif speed * BRAKE_TIME >= distance:
        print("BRAKE")
    # If we're not near a collision, we are SAFE
    else:
        print("SAFE")

