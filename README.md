# Not So Self Driving

## Useful Links
- [LM Codequest Academy Problem: Not So
  Self-Driving](https://lmcodequestacademy.com/problem/not-so-self-driving)
## Tips
- This problem wants us to use if-elif-else statements, as stated in the
  problem guide. Before trying to code this problem, try writing your solution
  using the following prompt:
```
If the car is going to hit an obstacle in ________ seconds, the program should
print ______
Otherwise, if the car is going to hit an obstacle in ________ seconds, the
program should print ______
Otherwise, the program should print ______
```
## Legal
All exercises are property of LM Codequest Academy and are used here purely for
educational purposes.
